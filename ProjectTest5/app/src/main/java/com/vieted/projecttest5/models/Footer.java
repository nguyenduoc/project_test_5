package com.vieted.projecttest5.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Smile on 5/9/2016.
 */
public class Footer implements Parcelable {
    private String name  = "This is Footer";

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public Footer() {
    }

    protected Footer(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Footer> CREATOR = new Parcelable.Creator<Footer>() {
        @Override
        public Footer createFromParcel(Parcel source) {
            return new Footer(source);
        }

        @Override
        public Footer[] newArray(int size) {
            return new Footer[size];
        }
    };
}
