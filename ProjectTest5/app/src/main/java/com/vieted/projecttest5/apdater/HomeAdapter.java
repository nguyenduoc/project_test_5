package com.vieted.projecttest5.apdater;

import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.vieted.projecttest5.models.Footer;
import com.vieted.projecttest5.models.Header;
import com.vieted.projecttest5.models.Player;
import com.vieted.projecttest5.views.ClubView;
import com.vieted.projecttest5.models.Club;
import com.vieted.projecttest5.views.FooterView;
import com.vieted.projecttest5.views.HeaderView;
import com.vieted.projecttest5.views.PlayerView;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter {
    private static final int PARENT = 1;
    private static final int CHILD = 2;
    private static final int HEARD = 3;
    private static final int FOOTER = 4;

    private ArrayList<Parcelable> mItems;


    public HomeAdapter(ArrayList<Club> clubs) {
        super();
        if (clubs == null || clubs.size() == 0) {
            return;
        }
        mItems = new ArrayList<>();
        mItems.add(new Header());
        for (Club club : clubs) {
            if (club != null) {
                mItems.add(club);
                mItems.addAll(club.getPlayers());
            }
        }
        mItems.add(new Footer());

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEARD)
            return new AppViewHolder(new HeaderView(parent.getContext()));
        else if (viewType == PARENT)
            return new AppViewHolder(new ClubView(parent.getContext()));
        else if (viewType == CHILD)
            return new AppViewHolder(new PlayerView(parent.getContext()));
        else if (viewType == FOOTER)
            return new AppViewHolder(new FooterView(parent.getContext()));
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Object item = mItems.get(position);
        if (item == null) {
            return;
        }
        if (item instanceof Club) {
            ((ClubView) holder.itemView).display((Club) item);
        } else if (item instanceof Player) {
            ((PlayerView) holder.itemView).display((Player) item);
        } else if (item instanceof Header) {
            ((HeaderView) holder.itemView).display((Header) item);
        } else if (item instanceof Footer) {
            ((FooterView) holder.itemView).display((Footer) item);
        }
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = mItems.get(position);
        int type = 0;
        if (item instanceof Club) {
            type = PARENT;
        } else if (item instanceof Player) {
            type = CHILD;
        } else if (item instanceof Header) {
            type = HEARD;
        } else if (item instanceof Footer) {
            type = FOOTER;
        }
        return type;
    }

    public Parcelable getItem(int position) {
        return (mItems != null && position < mItems.size()) ? mItems.get(position) : null;
    }
}
