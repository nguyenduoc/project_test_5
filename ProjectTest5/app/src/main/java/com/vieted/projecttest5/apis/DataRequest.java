package com.vieted.projecttest5.apis;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vieted.projecttest5.models.Club;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Smile on 3/6/2016.
 */
public class DataRequest extends AsyncTask<Void, Void,ArrayList<Club>> {
    public static final String URL_API = "https://api.myjson.com/bins/usgc";
    private OnDataChangedListener<ArrayList<Club>> mDataChangedListener;
    private OnErrorListener mOnErrorListener;
    private Handler mHandler;
    private boolean requesting = false;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        destroy(mHandler);
        mHandler = new Handler();
        requesting = true;
        Log.e("ND", "on pre excute");
    }

    @Override
    protected ArrayList<Club> doInBackground(Void... params) {
        String json = makeHttp();
        Gson gson = new Gson();
        Log.e("ND", "on pre doInBackground");
        return gson.fromJson(json, new TypeToken<ArrayList<Club>>(){}.getType());
    }

    private void pushError(final String msg) {
        if (mHandler == null) return;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnErrorListener != null)
                    mOnErrorListener.error(msg);
            }
        });

    }

    @Override
    protected void onPostExecute(ArrayList<Club> clubs) {
        super.onPostExecute(clubs);
        Log.e("ND", "on pre onPostExecute");
        requesting = false;
        if (mDataChangedListener != null)
            mDataChangedListener.dataChanged(clubs);

    }

    public void startRequest() {
        execute();
    }

    public boolean isRequesting() {
        return requesting;
    }

    public void cancelRequest() {
        destroy(mHandler);
        cancel(true);

    }

    public void setOnDataChangedListener(OnDataChangedListener<ArrayList<Club>> listener) {
        this.mDataChangedListener = listener;
    }

    public void setOnErrorListener(OnErrorListener listener) {
        this.mOnErrorListener = listener;
    }

    private String makeHttp() {
        Log.v("ND", "Run loading data to internet");
        URL url;
        HttpURLConnection connection = null;
        String json = null;
        try {
            url = new URL(URL_API);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(10000);
            connection.setDoInput(true);
            InputStream in = new BufferedInputStream(connection
                    .getInputStream());
            json = getStringFromInputStream(in);
        } catch (IOException e) {
            pushError(e.getMessage());
        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return json;
    }

    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            pushError(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public void destroy(Handler handler) {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

}
