package com.vieted.projecttest5.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vieted.projecttest5.R;
import com.vieted.projecttest5.apdater.HomeAdapter;
import com.vieted.projecttest5.apdater.OnItemTouchListener;
import com.vieted.projecttest5.apis.DataRequest;
import com.vieted.projecttest5.apis.OnDataChangedListener;
import com.vieted.projecttest5.apis.OnErrorListener;
import com.vieted.projecttest5.models.Club;

import java.util.ArrayList;


/**
 * Created by Smile on 3/31/2016.
 */
public class HomeFragment extends Fragment implements OnDataChangedListener<ArrayList<Club>>, OnErrorListener {

    private static final String SAVE_MODEL = "save_model";
    private static final String IS_GET_DATA = "is_get_data";
    private ArrayList<Club> mClubs;
    private DataRequest mDataRequest;
    private boolean isGetData = true;

    private RecyclerView mRecyclerView;
    private View mLoading;

    private HomeAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            restoreData(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        mLoading = view.findViewById(R.id.view_loading);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addOnItemTouchListener(new OnItemTouchListener(getContext()) {
            @Override
            public void onItemTouch(int position) {
                if (mAdapter != null) {
                    Parcelable item = mAdapter.getItem(position);
                    openDetailFragment(item);
                }
            }
        });
    }

    private void openDetailFragment(Parcelable item) {
        if (item == null) {
            return;
        }
        DetailFragment fragment = DetailFragment.newInstance(item);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.e("ND", ">>>>> activity create");
        super.onActivityCreated(savedInstanceState);
        if (mClubs != null && mClubs.size() > 0 && !isGetData)
            refreshData();
        else
            requestData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SAVE_MODEL, mClubs);
        if (mDataRequest != null && mDataRequest.isRequesting())
            outState.putBoolean(IS_GET_DATA, mDataRequest.isRequesting());
    }

    @Override
    public void onDestroy() {
        if (mDataRequest != null) {
            mDataRequest.cancelRequest();
        }
        super.onDestroy();
    }

    private void refreshData() {
        isGetData = false;
        hideLoading();
        if (mClubs != null) {
            mAdapter = new HomeAdapter(mClubs);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private void restoreData(Bundle bundle) {
        if (bundle != null) {
            mClubs =  bundle.getParcelableArrayList(SAVE_MODEL);
            isGetData = bundle.getBoolean(IS_GET_DATA);
        }
    }

    private void requestData() {
        showLoading();
        if (mDataRequest != null)
            mDataRequest.cancelRequest();
        mDataRequest = new DataRequest();
        mDataRequest.setOnDataChangedListener(this);
        mDataRequest.setOnErrorListener(this);
        mDataRequest.startRequest();
    }

    @Override
    public void dataChanged(ArrayList<Club> clubs) {
        mClubs = clubs;
        refreshData();
    }

    @Override
    public void error(String message) {
        hideLoading();
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    public void showLoading() {
        if (mLoading != null)
            mLoading.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        if (mLoading != null)
            mLoading.setVisibility(View.INVISIBLE);
    }
}
