package com.vieted.projecttest5.apis;

/**
 * Created by Smile on 3/31/2016.
 */
public interface ApiKey {
    String NAME = "name";
    String POSITION = "position";
    String AGE = "age";
    String COUNT_NUMBER = "count_member";
    String PLAYER = "players";
}
