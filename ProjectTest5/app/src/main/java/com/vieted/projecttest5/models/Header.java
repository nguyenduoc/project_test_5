package com.vieted.projecttest5.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Smile on 5/9/2016.
 */
public class Header implements Parcelable {
    private String name  = "This is Header";

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
    }

    public Header() {
    }

    protected Header(Parcel in) {
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Header> CREATOR = new Parcelable.Creator<Header>() {
        @Override
        public Header createFromParcel(Parcel source) {
            return new Header(source);
        }

        @Override
        public Header[] newArray(int size) {
            return new Header[size];
        }
    };
}
