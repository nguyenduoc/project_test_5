package com.vieted.projecttest5.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vieted.projecttest5.R;
import com.vieted.projecttest5.models.Club;

/**
 * Created by Smile on 5/9/2016.
 */
public class ClubView extends RelativeLayout {
    private TextView mViewName;

    public ClubView(Context context) {
        super(context);
        init(context);
    }

    public ClubView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClubView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ClubView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.item_list, this);
        mViewName = (TextView) findViewById(R.id.name);
    }

    public void display(Club club) {
        String name = "";
        if (club != null && club.getName() != null) {
            name = club.getName();
        }
        this.mViewName.setText(name);
        mViewName.setTextColor(Color.WHITE);
        setBackgroundColor(Color.BLUE);
    }
}
