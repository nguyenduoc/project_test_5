package com.vieted.projecttest5.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.vieted.projecttest5.apis.ApiKey;

import java.util.ArrayList;

/**
 * Created by Smile on 5/9/2016.
 */
public class Club implements Parcelable {

    @SerializedName(ApiKey.NAME)
    private String name;
    @SerializedName(ApiKey.COUNT_NUMBER)
    private int countNumber;
    @SerializedName(ApiKey.PLAYER)
    private ArrayList<Player> players;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.countNumber);
        dest.writeTypedList(players);
    }

    public Club() {
    }

    protected Club(Parcel in) {
        this.name = in.readString();
        this.countNumber = in.readInt();
        this.players = in.createTypedArrayList(Player.CREATOR);
    }

    public static final Parcelable.Creator<Club> CREATOR = new Parcelable.Creator<Club>() {
        @Override
        public Club createFromParcel(Parcel source) {
            return new Club(source);
        }

        @Override
        public Club[] newArray(int size) {
            return new Club[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getCountNumber() {
        return countNumber;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }
}
