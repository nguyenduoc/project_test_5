package com.vieted.projecttest5.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.vieted.projecttest5.apis.ApiKey;

/**
 * Created by Smile on 5/9/2016.
 */
public class Player implements Parcelable {
    @SerializedName(ApiKey.NAME)
    private String name;
    @SerializedName(ApiKey.POSITION)
    private String position;
    @SerializedName(ApiKey.AGE)
    private int age;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.position);
        dest.writeInt(this.age);
    }

    public Player() {
    }

    protected Player(Parcel in) {
        this.name = in.readString();
        this.position = in.readString();
        this.age = in.readInt();
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel source) {
            return new Player(source);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public int getAge() {
        return age;
    }
}

