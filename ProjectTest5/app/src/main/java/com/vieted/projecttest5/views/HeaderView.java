package com.vieted.projecttest5.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vieted.projecttest5.models.Header;
import com.vieted.projecttest5.R;

/**
 * Created by Smile on 5/9/2016.
 */
public class HeaderView extends RelativeLayout {
    private TextView mViewName;

    public HeaderView(Context context) {
        super(context);
        init(context);
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public HeaderView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.header_view, this);
        mViewName = (TextView) findViewById(R.id.name);
    }

    public void display(Header header) {
        String name = "";
        if (header != null && header.getName() != null) {
            name = header.getName();
        }
        this.mViewName.setText(name);
    }
}
