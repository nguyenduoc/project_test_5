package com.vieted.projecttest5.apis;

/**
 * Created by Smile on 3/6/2016.
 */
public interface OnErrorListener {
    void error(String message);
}
