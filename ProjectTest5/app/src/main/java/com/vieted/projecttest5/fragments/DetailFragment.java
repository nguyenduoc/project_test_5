package com.vieted.projecttest5.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vieted.projecttest5.models.Footer;
import com.vieted.projecttest5.models.Header;
import com.vieted.projecttest5.models.Player;
import com.vieted.projecttest5.R;
import com.vieted.projecttest5.models.Club;

import java.util.Locale;

/**
 * Created by Smile on 3/6/2016.
 */
public class DetailFragment extends Fragment {
    private static final String EXTRA_ITEM = "extra_item";
    private TextView mViewContent;
    private Parcelable mContent;

    public static DetailFragment newInstance(Parcelable item) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_ITEM, item);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            getDataFromBundle(savedInstanceState);
        } else {
            getDataFromBundle(getArguments());
        }
    }

    private void getDataFromBundle(Bundle bundle) {
        if (bundle != null) {
            mContent = bundle.getParcelable(EXTRA_ITEM);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_ITEM, mContent);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewContent = (TextView) view.findViewById(R.id.content);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mContent == null) {
            return;
        }
        if (mContent instanceof Header) {
            displayHeader((Header) mContent);
        } else if (mContent instanceof Footer) {
            displayFooter((Footer) mContent);
        } else if (mContent instanceof Club) {
            displayClub((Club) mContent);
        } else if (mContent instanceof Player) {
            displayPlayer((Player) mContent);
        }
    }

    private void displayPlayer(Player player) {
        String text = "";
        if (player != null && player.getName() != null && player.getPosition() != null) {
            text = String.format(Locale.ENGLISH, "Name: %s \nPosition: %s \nAge: %d", player.getName(), player.getPosition(), player.getAge());
        }
        mViewContent.setText(text);
    }

    private void displayClub(Club club) {
        String text = "";
        if (club != null) {
            if (club.getName() != null) {
                text += String.format(Locale.ENGLISH, "Club: %s", club.getName());
            }
            text += String.format(Locale.ENGLISH, "\nCount number: %d", club.getCountNumber());
        }
        mViewContent.setText(text);
    }

    private void displayFooter(Footer footer) {
        String text = "";
        if (footer.getName() != null) {
            text = footer.getName();
        }
        mViewContent.setText(text);
    }

    private void displayHeader(Header header) {
        String text = "";
        if (header.getName() != null) {
            text = header.getName();
        }
        mViewContent.setText(text);
    }
}
